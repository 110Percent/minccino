import { Sequelize, Model, DataTypes } from 'sequelize';
import path from 'path';
import fs from 'fs';
import ability from './init/ability';
import eggGroup from './init/eggGroup';
import pokeType from './init/pokeType';
import pokemon from './init/pokemon';
import typeDamage from './init/typeDamage';
import item from './init/item';
import move from './init/move';
import moveTarget from './init/moveTarget';

const inits: {[key: string]: Function} = {
  moveTarget,
  move,
  item,
  pokeType,
  typeDamage,
  ability,
  eggGroup,
  pokemon
};

// TODO: Don't bake these for the love of god
const sequelize = new Sequelize('minccino', 'root', 'pissword', {
  host: 'db',
  dialect: 'mysql',
  define: {
    freezeTableName: true
  },
  logging: false
});

const dbInit = async () => {

  try {
    await sequelize.authenticate();
  } catch (err) {
    console.error('Unable to connect to the database:', err);
  }
  console.log('Connection has been established successfully.');
  var models: { [key: string]: any } = {};
  fs.readdirSync(path.join(__dirname, '../../models/')).forEach(async function (filename) {
    var model: { [key: string]: any } = {};
    model.path = path.join(__dirname, '../../models/', filename)
    if (!model.path.endsWith('.js')) {
      return;
    }
    model.name = filename.replace(/\.[^/.]+$/, "");
    model.resource = await sequelize.import(model.path);
    model.resource.drop();
    models[model.name] = model;
  });
  await sequelize.sync({ force: true });
  for (let init in inits) {
    if (models[init]) {
      await inits[init](models[init].resource)
    }
  }
}

export default sequelize;
export { dbInit };