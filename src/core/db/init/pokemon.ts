import { Pokedex } from '../../../external/showdown/pokedex';
import { TypeChart } from '../../../external/showdown/typechart';
import { Abilities } from '../../../external/showdown/abilities';
import { FormatsData } from '../../../external/showdown/formats-data';
import { Model } from 'sequelize/types';
import { readFileSync } from 'fs';
import { join } from 'path';
import parse, { Parser } from 'csv-parse';
import assert from 'assert';

const colors: string[] = [];
const entries: string[] = [];

const csvs = {
  dexEntries: {
    path: 'pokemon_species_flavor_text.csv'
  }
};

export default async function (model: any) {

  const swshEntries = JSON.parse(readFileSync(join(__dirname, '../../../external/itsjavi/pokemon.json'), 'utf-8'));

  const parsers: { [key: string]: Parser } = {};
  for (let n in csvs) {
    // @ts-ignore
    parsers[n] = parse(readFileSync(join(__dirname, `../../../external/veekun/${csvs[n].path}`)), {
      columns: true,
      delimiter: ','
    });
  }

  let thisID = 1;
  let lastDesc = '';
  for await (const entry of parsers.dexEntries) {
    if (entry.language_id !== '9') continue;
    if (Number(entry.species_id) <= thisID) {
      lastDesc = entry.flavor_text;
    } else {
      entries.push(lastDesc.replace(/[\n]/g, ' '));
    }
    thisID = entry.species_id;
  }

  const keys = { '0': 'ability_1', '1': 'ability_2', H: 'ability_h' };
  const rawGroups: {
    id: number,
    name: string,
    createdAt: Date,
    updatedAt: Date
  }[] = await model.sequelize.modelManager.getModel('EggGroups').findAll({ raw: true });

  for (let p = 0; p < Object.keys(Pokedex).length; p++) {
    let pokemon = Object.values(Pokedex)[p];
    let format = FormatsData[Object.keys(Pokedex)[p]];

    if (pokemon.evos) {
      for (let evo of pokemon.evos) {
        await model.sequelize.modelManager.getModel('EvoRelations').create({
          prevo: pokemon.name,
          evo
        });
      }
    }

    let gender = 0.5;
    if (pokemon.genderRatio) {
      gender = pokemon.genderRatio.M;
    } else if (pokemon.gender) {
      if (pokemon.gender === 'F') {
        gender = 0;
      } else if (pokemon.gender === 'M') {
        gender = 1;
      } else {
        gender = -1;
      }
    }

    const abilities: { [key: string]: number | null } = {
      ability_1: null,
      ability_2: null,
      ability_h: null
    };
    for (const i in keys) {
      if (pokemon.abilities[i]) {
        // @ts-ignore
        abilities[keys[i]] = Object.values(Abilities)[
          Object.values(Abilities).findIndex(a => {
            return a.name === pokemon.abilities[i];
          })].num
      }
    }

    const groups: { [key: string]: number | null } = {
      egg_1: null,
      egg_2: null
    };
    for (let i = 0; i < pokemon.eggGroups.length; i++) {
      groups[`egg_${i + 1}`] = rawGroups[rawGroups.findIndex((g => g.name === pokemon.eggGroups[i]))].id
    }

    if (colors.indexOf(pokemon.color) < 0) {
      await model.sequelize.modelManager.getModel('Colors').create({ name: pokemon.color });
      colors.push(pokemon.color);
    }

    let bst = pokemon.baseStats;

    const swshIndex = swshEntries.findIndex((p: any) => {
      return Object.keys(p).length > 0
        && p.description !== null
        && !p.description.startsWith('Evolves')
        && p.types.toString() === Object.values(pokemon.types).toString()
        && p.height === pokemon.heightm
        && p.weight === pokemon.weightkg
        && p.base_stats.toString() === [bst.hp, bst.atk, bst.def, bst.spa, bst.spd, bst.spe].toString();
    });
    let dexEntry: string | null = null;
    if (swshIndex > -1) {
      dexEntry = swshEntries[swshIndex].description;
    } else {
      dexEntry = entries[pokemon.num - 1];
    }

    await model.create({
      name: pokemon.name,
      species_id: pokemon.num,
      type1: Object.keys(TypeChart).indexOf(pokemon.types[0]) + 1,
      type2: pokemon.types[1] ? Object.keys(TypeChart).indexOf(pokemon.types[1]) + 1 : null,
      male_ratio: gender * 10,
      ...pokemon.baseStats,
      ...abilities,
      height_m: pokemon.heightm,
      weight_kg: pokemon.weightkg,
      ...groups,
      color: colors.indexOf(pokemon.color) + 1,
      dex_entry: dexEntry,
      tier: (format && format.tier) ? format.tier : null,
      nonstandard: (format && format.isNonstandard) ? format.isNonstandard : null
    });
    console.log(`Imported Pokemon ${pokemon.name}`)
  }
}