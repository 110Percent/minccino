import parse, { Parser } from 'csv-parse';
import { readFileSync } from 'fs';
import { join } from 'path';
import { Moves } from '../../../external/showdown/moves';
import { TypeChart } from '../../../external/showdown/typechart';

const csvs = {
  name: {
    path: 'move_names.csv'
  },
  flavour: {
    path: 'move_flavor_text.csv'
  }
};

export default async function (model: any) {
  const targets = (await model.sequelize.modelManager.getModel('MoveTargets').findAll({ raw: true })).map((t: any) => t.comp_name);
  const compiled: { [key: string]: any }[] = [];
  const parsers: { [key: string]: Parser } = {};

  for (let n in csvs) {
    // @ts-ignore
    parsers[n] = parse(readFileSync(join(__dirname, `../../../external/veekun/${csvs[n].path}`)), {
      columns: true,
      delimiter: ','
    });
  }
  for await (const move of parsers.name) {
    if (move.local_language_id != 9) continue;
    compiled.push({
      move_id: Number(move.move_id),
      name: move.name.replace(/’/g, '\'')
    });
  }
  let thisID = 1;
  let lastDesc = '';
  for await (const move of parsers.flavour) {
    if (move.language_id !== '9') continue;
    if (Number(move.move_id) <= thisID) {
      lastDesc = move.flavor_text;
    } else {
      compiled[compiled.findIndex(c => c.move_id === Number(thisID))].desc = lastDesc.replace(/[\n]/g, ' ');
    }
    thisID = move.move_id;
  }

  let nonIndex = -1;
  const sorted = Object.values(Moves).sort((a, b) => a.num - b.num);
  for (let i = 0; i < sorted.length; i++) {
    const move = sorted[i];
    const modIndex = compiled.findIndex(c => c.name === move.name || (move.name === 'Vise Grip' && c.name === 'Vice Grip')); // why
    if (modIndex < 0) {
      compiled.push({
        name: move.name,
        comp_desc: move.desc,
        comp_short_desc: move.shortDesc,
        move_type: Object.keys(TypeChart).indexOf(move.type) + 1,
        pp: move.pp,
        base_power: move.basePower,
        accuracy: move.accuracy,
        priority: move.priority,
        target: targets.indexOf(move.target) + 1,
        category: move.category
      });
      nonIndex--;
    } else {
      compiled[modIndex] = Object.assign(compiled[modIndex], {
        comp_desc: move.desc,
        comp_short_desc: move.shortDesc,
        move_type: Object.keys(TypeChart).indexOf(move.type) + 1,
        pp: move.pp,
        base_power: move.basePower,
        accuracy: move.accuracy,
        priority: move.priority,
        target: targets.indexOf(move.target) + 1,
        category: move.category
      });
    }

  }

  for await (const move of compiled) {
    await model.create(move);
  }
}