import { TypeChart } from '../../../external/showdown/typechart';
import { Model } from 'sequelize/types';

const MULTIS = [ 1, 2, 0.5, 0 ];

export default async function (model: any) {
  for (const tName in TypeChart) {
    const damages = TypeChart[tName].damageTaken;
    for (const atk in damages) {
      if (Object.keys(TypeChart).indexOf(atk) > -1) {
        await model.create({
          atk: Object.keys(TypeChart).indexOf(atk) + 1,
          def: Object.keys(TypeChart).indexOf(tName) + 1,
          multiplier: MULTIS[damages[atk]]
        })
      }
    }
    console.log(`Imported Type ${tName}`)
  };
}