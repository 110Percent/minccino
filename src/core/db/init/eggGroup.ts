import { Model } from 'sequelize/types';
import { Pokedex } from '../../../external/showdown/pokedex';

export default async function (model: any) {
  const dupes = Object.values(Pokedex).map(p => p.eggGroups);
  console.log(dupes);
  const groups: string[] = [];
  for (let i = 0; i < dupes.length; i++) {
    for (let j = 0; j < dupes[i].length; j++) {
      if (groups.indexOf(dupes[i][j]) < 0) {
        groups.push(dupes[i][j]);
      }
    }
  }
  groups.sort((a, b) => a.localeCompare(b));
  for (let i = 0; i < groups.length; i++) {
    await model.create({
      name: groups[i]
    });
    console.log(`Imported Egg Group ${groups[i]}`)
  }
}