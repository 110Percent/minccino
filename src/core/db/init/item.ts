import { Items } from '../../../external/showdown/items';
import parse, { Parser } from 'csv-parse';
import { readFileSync } from 'fs';
import { join } from 'path';

const csvs = {
  name: {
    path: 'item_names.csv',
    initial: true,
    mapValues: {
      name: 'name',
      item_id: 'item_id'
    }
  },
  flavour: {
    path: 'item_flavor_text.csv',
    mapValues: {
      desc: 'flavor_text'
    }
  }
}

export default async function (model: any) {
  const compiled: { [key: string]: any }[] = [];

  const parsers: { [key: string]: Parser } = {};
  for (let n in csvs) {
    // @ts-ignore
    parsers[n] = parse(readFileSync(join(__dirname, `../../../external/veekun/${csvs[n].path}`)), {
      columns: true,
      delimiter: ','
    });
  }
  for await (const item of parsers.name) {
    if (item.local_language_id != 9) continue;
    compiled.push({
      item_id: Number(item.item_id),
      name: item.name.replace(/’/g, '\'')
    });
  }
  let thisID = 1;
  let lastDesc = '';
  for await (const item of parsers.flavour) {
    if (item.language_id !== '9') continue;
    if (Number(item.item_id) <= thisID) {
      lastDesc = item.flavor_text;
    } else {
      compiled[compiled.findIndex(c => c.item_id === Number(thisID))].desc = lastDesc.replace(/[\n]/g, ' ');
    }
    thisID = item.item_id;
  }

  let nonIndex = -1;
  const sorted = Object.values(Items).sort((a, b) => a.num - b.num);
  for (let i = 0; i < sorted.length; i++) {
    const item = sorted[i];
    const modIndex = compiled.findIndex(c => c.name === item.name);
    if (modIndex < 0) {
      compiled.push({
        item_id: nonIndex,
        name: item.name,
        comp_desc: item.desc
      });
      nonIndex--;
    } else {
      compiled[modIndex].comp_desc = item.desc;
    }

  }

  for await (const item of compiled) {
    await model.create(item);
  }
}