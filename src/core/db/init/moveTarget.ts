const targets = [
  {
    name: 'Any Pok\u00e9mon',
    comp_name: 'normal',
  },
  {
    name: 'User',
    comp_name: 'self',
  },
  {
    name: 'One Adjacent Ally',
    comp_name: 'adjacentAlly',
  },
  {
    name: 'User or Adjacent Ally',
    comp_name: 'adjacentAllyOrSelf',
  },
  {
    name: 'One Adjacent Opponent',
    comp_name: 'adjacentFoe',
  },
  {
    name: 'All Adjacent Opponents',
    comp_name: 'allAdjacentFoes',
  },
  {
    name: 'Opposing Side',
    comp_name: 'foeSide',
  },
  {
    name: 'User\'s Side',
    comp_name: 'allySide',
  },
  {
    name: 'User\'s Party',
    comp_name: 'allyTeam',
  },
  {
    name: 'All Adjacent Pok\u00e9mon',
    comp_name: 'allAdjacent',
  },
  {
    name: 'Any Pok\u00e9mon',
    comp_name: 'any',
  },
  {
    name: 'All Pok\u00e9mon',
    comp_name: 'all',
  },
  {
    name: 'Chosen Automatically',
    comp_name: 'scripted',
  },
  {
    name: 'Random Adjacent Opponent',
    comp_name: 'randomNormal',
  },
  {
    name: 'User and Allies',
    comp_name: 'allies',
  }
];

export default async function (model: any) {
  for await (const target of targets) {
    await model.create(target);
  }
}