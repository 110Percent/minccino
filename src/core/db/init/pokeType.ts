import { TypeChart } from '../../../external/showdown/typechart';
import { Model } from 'sequelize/types';

export default async function (model: any) {
  for (let i = 0; i < Object.keys(TypeChart).length; i++) {
    const tName = Object.keys(TypeChart)[i];
    await model.create({
      name: tName
    });
    console.log(`Imported Type ${tName}`)
  }
}