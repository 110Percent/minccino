import { Model } from 'sequelize/types';
import { Abilities } from '../../../external/showdown/abilities';

export default async function (model: any) {
  const sorted = Object.values(Abilities).sort((a, b) => a.num - b.num);
  for (let i = 0; i < sorted.length; i++) {
    const ability = sorted[i];
    await model.create({
      name: ability.name,
      desc: ability.desc,
      short_desc: ability.shortDesc,
      rating: ability.rating * 10,
      num: ability.num
    });
    console.log(`Imported Ability ${ability.name}`)
  }
}