import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const PokemonType = sequelize.define('PokeTypes', {
		name: { type: DataTypes.STRING(32) }
	});

	return PokemonType;
}