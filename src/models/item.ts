import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const Item = sequelize.define('Items', {
    item_id: { type: DataTypes.SMALLINT },
    name: { type: DataTypes.STRING(32) },
    desc: { type: DataTypes.TEXT },
    comp_desc: { type: DataTypes.TEXT }
	});

	return Item;
}