import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const Target = sequelize.define('MoveTargets', {
    name: { type: DataTypes.STRING(32) },
    comp_name: { type: DataTypes.STRING(32) }
	});

	return Target;
}