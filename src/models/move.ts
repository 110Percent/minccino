import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const Move = sequelize.define('Moves', {
    move_id: { type: DataTypes.SMALLINT },
		name: { type: DataTypes.STRING },
    desc: { type: DataTypes.TEXT },
    comp_desc: { type: DataTypes.TEXT },
    comp_short_desc: { type: DataTypes.TEXT },
    move_type: { type: DataTypes.TINYINT },
    category: { type: DataTypes.STRING(12) },
    pp: { type: DataTypes.TINYINT },
    base_power: { type: DataTypes.SMALLINT },
    accuracy: { type: DataTypes.TINYINT },
    target: { type: DataTypes.TINYINT },
    priority: { type: DataTypes.TINYINT }
	});

	return Move;
}