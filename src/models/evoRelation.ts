import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const Evo = sequelize.define('EvoRelations', {
        prevo: { type: DataTypes.STRING(32) },
        evo: { type: DataTypes.STRING(32) }
	});

	return Evo;
}