import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const Pokemon = sequelize.define('Pokemon', {
		name: { type: DataTypes.STRING },
		species_id: { type: DataTypes.SMALLINT },
		type1: { type: DataTypes.TINYINT },
		type2: { type: DataTypes.TINYINT },
		male_ratio: { type: DataTypes.DECIMAL(3, 1) },
		hp: { type: DataTypes.SMALLINT },
		atk: { type: DataTypes.SMALLINT },
		def: { type: DataTypes.SMALLINT },
		spa: { type: DataTypes.SMALLINT },
		spd: { type: DataTypes.SMALLINT },
		spe: { type: DataTypes.SMALLINT },
		ability_1: { type: DataTypes.SMALLINT },
		ability_2: { type: DataTypes.SMALLINT },
		ability_h: { type: DataTypes.SMALLINT },
		height_m: { type: DataTypes.DECIMAL(4, 1) },
		weight_kg: { type: DataTypes.DECIMAL(5, 1) },
		color: { type: DataTypes.TINYINT },
		egg_1: { type: DataTypes.TINYINT },
		egg_2: { type: DataTypes.TINYINT },
		dex_entry: { type: DataTypes.TEXT },
		nonstandard: { type: DataTypes.STRING },
		tier: { type: DataTypes.STRING(12) }
	});

	return Pokemon;
}