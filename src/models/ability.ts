import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const Ability = sequelize.define('Abilities', {
    num: { type: DataTypes.SMALLINT },
		name: { type: DataTypes.STRING },
    desc: { type: DataTypes.TEXT },
    short_desc: { type: DataTypes.STRING },
    rating: { type: DataTypes.TINYINT }
	});

	return Ability;
}