import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const TypeDamage = sequelize.define('TypeDamage', {
    atk: { type: DataTypes.TINYINT },
    def: { type: DataTypes.TINYINT },
    multiplier: { type: DataTypes.DECIMAL(2, 1)}
	});

	return TypeDamage;
}