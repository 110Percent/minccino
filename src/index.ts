import db, { dbInit } from './core/db'
import fs from 'fs';
import path from 'path';

const world = 'world 🗺️';

export function hello(word: string = world): string {
  return `Hello ${world}! `;
}

dbInit();

console.log(hello(world));