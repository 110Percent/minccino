mkdir -p src/external
cd src/external


mkdir -p showdown

touch showdown/pokedex.ts
echo '// @ts-nocheck' > showdown/pokedex.ts
curl 'https://raw.githubusercontent.com/smogon/pokemon-showdown/master/data/pokedex.ts' >> showdown/pokedex.ts

touch showdown/typechart.ts
echo '// @ts-nocheck' > showdown/typechart.ts
curl 'https://raw.githubusercontent.com/smogon/pokemon-showdown/master/data/typechart.ts' >> showdown/typechart.ts

touch showdown/abilities.ts
echo '// @ts-nocheck' > showdown/abilities.ts
curl 'https://raw.githubusercontent.com/smogon/pokemon-showdown/master/data/abilities.ts' >> showdown/abilities.ts

touch showdown/items.ts
echo '// @ts-nocheck' > showdown/items.ts
curl 'https://raw.githubusercontent.com/smogon/pokemon-showdown/master/data/items.ts' >> showdown/items.ts

touch showdown/moves.ts
echo '// @ts-nocheck' > showdown/moves.ts
curl 'https://raw.githubusercontent.com/smogon/pokemon-showdown/master/data/moves.ts' >> showdown/moves.ts

touch showdown/formats-data.ts
echo '// @ts-nocheck' > showdown/formats-data.ts
curl 'https://raw.githubusercontent.com/smogon/pokemon-showdown/master/data/formats-data.ts' >> showdown/formats-data.ts


cd ../../

mkdir -p dist/external/veekun

touch dist/external/veekun/item_names.csv
curl 'https://raw.githubusercontent.com/veekun/pokedex/master/pokedex/data/csv/item_names.csv' > dist/external/veekun/item_names.csv
touch dist/external/veekun/item_flavor_text.csv
curl 'https://raw.githubusercontent.com/veekun/pokedex/master/pokedex/data/csv/item_flavor_text.csv' > dist/external/veekun/item_flavor_text.csv

touch dist/external/veekun/move_names.csv
curl 'https://raw.githubusercontent.com/veekun/pokedex/master/pokedex/data/csv/move_names.csv' > dist/external/veekun/move_names.csv
touch dist/external/veekun/move_flavor_text.csv
curl 'https://raw.githubusercontent.com/veekun/pokedex/master/pokedex/data/csv/move_flavor_text.csv' > dist/external/veekun/move_flavor_text.csv

touch dist/external/veekun/pokemon_species_flavor_text.csv
curl 'https://raw.githubusercontent.com/veekun/pokedex/master/pokedex/data/csv/pokemon_species_flavor_text.csv' > dist/external/veekun/pokemon_species_flavor_text.csv


mkdir -p dist/external/itsjavi
curl 'https://raw.githubusercontent.com/itsjavi/swordshield-data/master/data/json/pokemon.json' > dist/external/itsjavi/pokemon.json