FROM node

WORKDIR /usr/src/app

COPY package.json .
RUN npm install

ADD . /usr/src/app
RUN rm -rf /usr/src/app/dist

CMD [ "npm", "run", "full" ]